import Vue from 'vue';
import App from './App.vue';
import router from './router';

Vue.config.productionTip = false;

const requireComponent = require.context(
    // Look for files in the current directory
    './components',
    // Do not look in subdirectories
    false,
    // The regular expression used to match base component filenames
    /Base[A-Z]\w+\.(vue|js)$/,
);

// For each matching file name...
requireComponent.keys().forEach((fileName) => {
    // Get the component config
    const componentConfig = requireComponent(fileName);
    // remove extension
    const componentName = fileName.replace(/\.\w+$/, '').replace(/^\.\//, '');
    // Globally register the component
    Vue.component(componentName, componentConfig.default || componentConfig);
});

new Vue({
    router,
    render: h => h(App),
}).$mount('#app');
