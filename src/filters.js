function abcToNatural(value) {
    if (typeof value !== 'string') {
        throw new TypeError(`${value} is not a string`);
    }

    return value.toUpperCase()
        .replace(/[,']/g, '')
        .replace(/_(\w)/g, '$1♭')
        .replace(/\^(\w)/g, '$1♯')
        .replace(/=(\w)/g, '$1♮');
}

function camelCaseToSentenceCase(value) {
    if (typeof value !== 'string') {
        throw new TypeError(`${value} is not a string`);
    }

    return value
        .replace(/([a-z])([A-Z][a-z])/g, '$1 $2')
        .replace(/([A-Z][a-z])([A-Z])/g, '$1 $2')
        .replace(/([a-z])([A-Z]+[a-z])/g, '$1 $2')
        .replace(/([A-Z]+)([A-Z][a-z][a-z])/g, '$1 $2')
        .replace(/([a-z]+)([A-Z0-9]+)/g, '$1 $2')
        .replace(/([A-Z]+)([A-Z][a-rt-z][a-z]*)/g, '$1 $2')
        .replace(/([0-9])([A-Z][a-z]+)/g, '$1 $2')
        .replace(/([A-Z]{2,})([0-9]{2,})/g, '$1 $2')
        .replace(/([0-9]{2,})([A-Z]{2,})/g, '$1 $2')
        .trim();
}

function humanise(value) {
    if (typeof value !== 'string') {
        throw new TypeError(`${value} is not a string`);
    }

    let returnVal = camelCaseToSentenceCase(value);
    returnVal = returnVal.replace(/[_-]+/g, ' ').replace(/\s{2,}/g, ' ').trim();
    returnVal = returnVal.charAt(0).toUpperCase() + returnVal.slice(1);

    return returnVal;
}

function titleCase(value) {
    if (typeof value !== 'string') {
        throw new TypeError(`${value} is not a string`);
    }

    const returnVal = value.toLowerCase().split(' ');

    for (let i = 0; i < returnVal.length; i += 1) {
        returnVal[i] = returnVal[i].charAt(0).toUpperCase() + returnVal[i].slice(1);
    }

    return returnVal.join(' ');
}

export {
    abcToNatural,
    humanise,
    titleCase,
};
